#!/bin/bash
echo "Tool installation starts"
sudo apt-get update
sudo apt-get install npm -y -qq
sudo npm version
echo "---------------------------"
sudo npm install nodejs -y -qq
sudo nodejs -v
echo "---------------------------"
sudo npm install pm2 -y -qq
echo "---------------------------"
sudo apt update
sudo apt install docker.io -y
sudo usermod -aG docker vagrant
echo "Tool installation done"
